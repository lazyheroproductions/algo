#include "DirectedGraph.h"

using namespace std;

int DirectedGraph::fillFromFile(string fname){
    ifstream file;
    string line;
    file.open(fname.c_str());

    if (!file){
        cout << "Datei konnte nicht geöffnet werden" << endl;
        return 1;
    }

    int i = 0;
    string nodeFrom, nodeTo;
    float weight;

    while (!file.eof()){
        file >> line;

        switch(i%3){
            case 0:
                nodeFrom = line;
                break;
            case 1:
                nodeTo = line;
                break;
            case 2:
                weight = sToF(line);
                adjacentList[nodeFrom].push_back(Edge(nodeTo, weight));
                break;
        }
        i++;
    }

    return 0;
}

void DirectedGraph::findPath(string start, string target, int maxSteps){
    queue< vector<Edge> > paths;
    int step = 0;
    visited_t visited;

    //Iterate through all Adjacent Nodes of the StartNode
    for (int i=0; i < adjacentList[start].size(); i++){
        vector<Edge> newPath;
        newPath.push_back(adjacentList[start][i]);
        paths.push(newPath);
    }
    while(step < maxSteps){
        // save size, so it doesn't have to be recalculated every time
        int pathSize = (int)paths.size();
        for(int i=0; i<pathSize; i++){
            //Set Current values for this iteration
            vector<Edge> currPath = paths.front();
            string currentEdgeName = currPath.back().target;

            //Found Node
            if(currentEdgeName.compare(target) == 0){
                cout << start << " -> ";
                for(int i=0; i<currPath.size()-1; i++){
                    cout << currPath.at(i).target << " -> ";
                }

                cout << currPath.back().target << endl;
                return;
            }

            //Didn't find Node
            for (int j=0; j < adjacentList[currentEdgeName].size(); j++){
                //if the adjacentNode was already visited, there is no reason to process it again
                //Prevents infinite loops
                if (!visited[adjacentList[currentEdgeName][j].target]){
                    vector<Edge> newPath = currPath;
                    newPath.push_back(adjacentList[currentEdgeName][j]);
                    paths.push(newPath);

                    visited[adjacentList[currentEdgeName][j].target] = true;
                }
            }
            visited[currentEdgeName] = true;
            paths.pop();
        }
        step++;
    }
    cout << "Es wurde kein Pfad gefunden" << endl;
}

void DirectedGraph::dijkstraPath(string start, adjacentList_t adjacentList){
    visited_t visited;

    //Iterate through AdjacentList(map) and set all distances to Infinity
    for (adjacentList_t::iterator iter = adjacentList.begin();
         iter != adjacentList.end();
         iter++){
        minDistance[iter->first] = INF;
    }

    //Set Distance of start Node to 0
    minDistance[start] = 0;

    PriorityQueue queue;
    queue.Insert(Edge(start, 0));

    while(queue.Size() > 0){
        Edge current = queue.Minimum();
        queue.ExtractMin();
        string edgeName = current.target;
        float edgeWeight = current.weight;

        if (visited[edgeName]){
            //If node was already visited, go to beginning of loop
            continue;
        }else{
            visited[edgeName] = true;
            //Iterating through all adjacent Edges of the current Node
            for (int i=0; i < adjacentList[edgeName].size(); i++){
                //If Adjacent Node hasnt been visited yet, and the distance between the nodes, is smaller than previously recorded
                if (!visited[adjacentList[edgeName][i].target] &&
                    adjacentList[edgeName][i].weight+edgeWeight < minDistance[adjacentList[edgeName][i].target]){
                    //Set smaller value as new Min Distance
                    minDistance[adjacentList[edgeName][i].target] = adjacentList[edgeName][i].weight + edgeWeight;

                    previous[adjacentList[edgeName][i].target] = edgeName;

                    //Add new Edge to Queue
                    queue.Insert(Edge(adjacentList[edgeName][i].target, minDistance[adjacentList[edgeName][i].target]));
                }
            }
        }
    }

    //Print Results by iterating through the minDistance map
    for (map<string, float>::iterator iter = minDistance.begin();
         iter != minDistance.end();
         iter++){
        if (iter->second != INF){
            cout << "Von: " << start << "\tNach: " << left << setw(17) << iter->first << "Abstand: " << iter->second << endl;
        }else{
            cout << "Von: " << start << "\tNach: " << left << setw(17) << iter->first << "Abstand: " << "Keine Verbindung" << endl;
        }
    }

    disjktraShortestPathPrompt();
}

void DirectedGraph::printEdges(){
    for(adjacentList_t::iterator iter = adjacentList.begin();
         iter != adjacentList.end();
         iter++){
        cout << iter->first << ":" << endl;
        for (int i = 0; i < iter->second.size(); i++){
            cout << "[" << i << "]: " << left << setw(15) << iter->second.at(i).target << "\t" << iter->second.at(i).weight << endl;
        }
    }
}

list<string> DirectedGraph::dijkstraShortestPathTo(string target, previous_t previous){
    list<string> path;
    previous_t::iterator prev;
    string node = target;
    path.push_front(node);
    while((prev = previous.find(node)) != previous.end()){
        node = prev->second;
        path.push_front(node);
    }
    return path;
}

void DirectedGraph::startPrompt(){
    while(true){
        cout << "==============================" << endl;
        cout << "Was möchten Sie tun?:" << endl;
        cout << "[1]: Prüfe ob es einen Pfad zwischen Knoten x und ";
        cout << "Knoten y gibt, der über maximal n Kanten läuft." << endl;
        cout << "[2]: Disjktra Algorithmus" << endl;
        cout << "[3]: Beenden" << endl << endl;
        cout << "Ihre Wahl: ";
        int input;
        while (!(cin >> input)){
            cout << "Bitte eine gültige Zahl eingeben: ";
            cin.clear();
            cin.ignore(INT_MAX, '\n');
        }
        if(input == 1){
            findPathPrompt();
        }
        if(input == 2){
            disjktraPrompt();
        }
        if(input == 3)
            break;
    }
}

void DirectedGraph::findPathPrompt(){
    string from, to;
    int maxEdges;
    cout << "==============================" << endl;
    cout << "Startknoten eingeben: ";
    cin >> from;
    while (adjacentList.find(from) == adjacentList.end()){
        cout << "Ungültiger Knoten, bitte erneut eingeben: ";
        cin >> from;
    }
    cout << "Zielknoten eingeben: ";
    cin >> to;
    while (adjacentList.find(to) == adjacentList.end() || from.compare(to) == 0){
        cout << "Ungültiger Knoten, bitte erneut eingeben: ";
        cin >> to;
    }
    cout << "Maximale Anzahl der Knoten eingeben: ";
    while (!(cin >> maxEdges) || maxEdges < 0){
        cout << "Bitte eine gültige Zahl eingeben: ";
        cin.clear();
        cin.ignore(INT_MAX, '\n');
    }
    cout << endl;

    cout << "Von " << from << " zu " << to << " über maximal ";
    cout << maxEdges << " Knoten:" << endl;

    findPath(from, to, maxEdges);
}

void DirectedGraph::disjktraPrompt(){
    string from, to;
    cout << "==============================" << endl;
    cout << "Startknoten eingeben: ";
    cin >> from;
    while (adjacentList.find(from) == adjacentList.end()){
        cout << "Ungültiger Knoten, bitte erneut eingeben: ";
        cin >> from;
    }

    dijkstraPath(from, adjacentList);
}

void DirectedGraph::disjktraShortestPathPrompt(){
    string to;
    cout << "==============================" << endl;
    cout << "Zielknoten eingeben: ";
    cin >> to;
    while (adjacentList.find(to) == adjacentList.end()){
        cout << "Ungültiger Knoten, bitte erneut eingeben: ";
        cin >> to;
    }

    list<string> path = dijkstraShortestPathTo(to, previous);

    if (minDistance[to] == INF){
        cout << "Keine Verbindung vorhanden" << endl;
    }else{
        int steps = 0;
        list<string>::iterator path_iter = path.begin();
        cout << "Weg: ";
        for( ; path_iter != path.end(); path_iter++){
            if (steps == 0){
                cout << *path_iter;
            }else{
                cout << " -> " << *path_iter;
            }

            steps++;
        }
        cout << endl << "Abstand: " << minDistance[to] << endl;
    }
}

float DirectedGraph::sToF(string s)
{
    float f = 0;;
    stringstream(s)>>f;
    return f;
}
