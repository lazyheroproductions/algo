#include "PriorityQueue.h"

using namespace std;

void PriorityQueue::Insert(Edge edge)
{
    heap.push_back(edge);
    MoveUp(Size()-1);
}

Edge PriorityQueue::Minimum()
{
    return heap.at(0);
}

void PriorityQueue::ExtractMin()
{
    swap(heap.at(0), heap.at(Size()-1));
    heap.pop_back();

    MoveDown(0);
}

void PriorityQueue::MoveDown(int index)
{
    int curIndex = index;

    while(curIndex < Size()-1)
    {
        int leftChild = curIndex * 2 + 1;
        int rightChild = curIndex * 2 + 2;
        int minIndex = curIndex;

        if (rightChild > Size()-1)
        {
            if (leftChild > Size()-1)
            {
                break;
            }
            else
            {
                if (heap.at(curIndex).weight > heap.at(leftChild).weight)
                {
                    minIndex = leftChild;
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            if (heap.at(leftChild).weight < heap.at(rightChild).weight)
            {
                if (heap.at(curIndex).weight > heap.at(leftChild).weight)
                {
                    minIndex = leftChild;
                }
                else
                {
                    break;
                }
            }
            else
            {
                if (heap.at(curIndex).weight > heap.at(rightChild).weight)
                {
                    minIndex = rightChild;
                }
                else
                {
                    break;
                }
            }
        }
        swap(heap.at(curIndex), heap.at(minIndex));
        curIndex = minIndex;
    }
}

void PriorityQueue::MoveUp(int index)
{
    int child = index;
    int parent = (child-1)/2;

    while (heap.at(parent).weight > heap.at(child).weight && parent >= 0)
    {
        swap(heap.at(parent), heap.at(child));

        child = parent;
        parent = (child-1)/2;
    }
}

void PriorityQueue::IncreaseSeq(string target, int d)
{
    int index = -1;
    for (int i=0; i < Size(); i++)
    {
        if (heap.at(i).target == target)
        {
            index = i;
        }
    }

    if (index >= 0)
    {
        if (d > 0)
        {
            MoveDown(index);
        }
        else if (d < 0)
        {
            MoveUp(index);
        }
    }
}

void PriorityQueue::DeleteSeq(string target)
{
    int index = -1;
    for (int i=0; i < Size(); i++)
    {
        if (heap.at(i).target == target)
        {
            index = i;
        }
    }

    if (index >= 0)
    {
        if (index != Size()-1)
        {
            swap(heap.at(index), heap.at(Size()-1));
        }
        heap.pop_back();

        MoveDown(index);
    }
}
