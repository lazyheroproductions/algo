#include <iostream>
#include "DirectedGraph.h"

using namespace std;

int main(int argc, const char * argv[])
{
    string controlFile;
    if (argc <= 1){
        //Keine Argumente wurden uebergeben
        cout << "Keine Steuerdatei uebergeben, wechsle zu Default: OS_Map.txt" << endl;
        controlFile = "OS_Map.txt";
    }else{
        controlFile = argv[1];
        cout << "Steuerdatei: " << controlFile << endl;
    }


    DirectedGraph dG;
    if (dG.fillFromFile(controlFile) != 0){
        return 1;
    }
    cout << "Datei " << controlFile << " erfolgreich eingelesen!" << endl;

    dG.printEdges();

    dG.startPrompt();

    return 0;
}
