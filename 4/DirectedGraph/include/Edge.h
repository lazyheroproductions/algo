#ifndef EDGE_H
#define EDGE_H

#include <string>

using namespace std;

class Edge
{
public:
    string target;
    double weight;
    Edge(string arg_target, double arg_weight) : target(arg_target), weight(arg_weight) { }
};

#endif
