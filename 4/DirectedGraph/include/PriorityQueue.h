#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <iostream>
#include <vector>
#include "Edge.h"
#include "DirectedGraph.h"

using namespace std;

class PriorityQueue{
private:
    vector<Edge> heap;
public:
    int Size(){ return (int)heap.size(); };

    void Insert(Edge edge);
    Edge Minimum();
    void ExtractMin();
    void MoveDown(int index);
    void MoveUp(int index);
    void IncreaseSeq(string target, int d);
    void DeleteSeq(string target);
    void PrintHeap();
};

#endif
