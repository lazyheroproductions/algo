#ifndef DIRECTEDGRAPH_H
#define DIRECTEDGRAPH_H

#include <fstream>
#include <iostream>
#include <climits>
#include <vector>
#include <map>
#include <iomanip>
#include <queue>
#include <list>
#include <sstream>
#include "Edge.h"
#include "PriorityQueue.h"

using namespace std;

//Saves Unnecessary Type Definitions
typedef map<string, vector<Edge> > adjacentList_t;
typedef map<string, bool> visited_t;
typedef map<string, float> minDistance_t;
typedef map<string, string> previous_t;
#define INF INT_MAX

class DirectedGraph
{

public:
    adjacentList_t adjacentList;
    minDistance_t minDistance;
    previous_t previous;

    DirectedGraph(){}
    virtual ~DirectedGraph(){}

    int fillFromFile(string fname);
    void findPath(string start, string target, int maxSteps);
    void dijkstraPath (string start, adjacentList_t adjacentList);
    void printEdges();
    list<string> dijkstraShortestPathTo(string target, previous_t previous);
    void startPrompt();
    void findPathPrompt();
    void disjktraPrompt();
    void disjktraShortestPathPrompt();
    float sToF(string s);
};

#endif
