#include <iostream>
#include <string>
#include <sstream>
#include "Utilities.h"
#include "Random.h"
#include "RBTree.h"
#include "Node.h"

void fillTree();
void shuffleArray(int *a, int n);

using namespace std;

int main(int argc, const char * argv[])
{
    cout << "Hello RBTree!" << endl;
    fillTree();
    return 0;
}

void fillTree(){
    // mit werten zwischen 0 und 1 000 000 füllen
    int length = 1000000;
    for(int j = 1; j < 6; j++){
        cout << "aufbau mit " << length*j << " elementen" << endl;
        RBTree *tree = new RBTree();

        cout << "permutieren" << endl;
        int *a = new int[length * j];
        shuffleArray(a, length * j);

        cout << "baum fuellen" << endl;
        clock_t start = clock();
        for(int i=0; i<length*j; i++) {
            stringstream s;
            s << "knoten_" << (a[i]/10);
            Node* n = new Node(a[i], s.str());
            n->leftChildNode = n->rightChildNode = n->parentNode = tree->nil;
            tree->insert(new Node(a[i], s.str()));
            //cout << a[i] << endl;
        }
        clock_t end = clock();
        cout << "zeit gestoppt" << endl;

        double t = (end-start) / 1000;
        cout << "in " << t << "ms gebaut" << endl;

        if(tree->check()){
            cout << "baum ist richtig" << endl;
        }else{
            cout << "baum ist nicht richtig" << endl;
        }
        cout << "hoehe: " << tree->height() << endl;
        delete tree;
        delete a;
        cout << "aufgeraeumt" << endl;
    }
}

void shuffleArray(int *a, int n){
    Random r(n-1);

    ///fill array
    for(int i = 0;i<n;i++)
    {
        a[i] = i;
    }
/*    for(int i = 0; i<n*10; i++)
    {
        swap(a[r.give()], a[r.give()]);
    } */
}
