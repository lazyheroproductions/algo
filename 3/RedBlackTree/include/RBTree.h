#ifndef RBTREE_H
#define RBTREE_H

#include "Node.h"
#include <cstddef>
#include <string>

class RBTree
{
    public:

        Node* nil = new Node(true);
        Node* rootNode = nil;

        // public methods
        void leftRotate(Node* n);
        void rightRotate(Node* n);
        void insert(Node *newNode);
        void repairInsert(Node* n);
        std::string search(int k);
        std::string search(Node *root, int k);
        bool check();
        bool checkRecursive(Node* node);
        int height();
        int heightRecursive(Node* noe);
        int max(int a, int b);
        void printTree(Node* node);
        Node* findLeftMin(Node* root);
        bool checkRedHasBlackChild(Node* root);
        bool checkProperty5(Node* root);
        bool checkProperty5Routine(Node* root, int blackCount, int *blackCountTotal);


    protected:
    private:

};

#endif // RBTREE_H
