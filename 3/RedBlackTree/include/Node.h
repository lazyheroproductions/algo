#ifndef NODE_H
#define NODE_H

#include <string>

class Node
{
    public:
        Node();
        Node(int k, std::string v);
        Node(bool isNill);

        bool hasChilds();

        //properties
        int key;
        Node* rightChildNode; // the bigger child
        Node* leftChildNode; // the smaller child
        Node* parentNode;
        std::string value;
        bool isRed;
        bool isNil;

    protected:
    private:

};

#endif // NODE_H
