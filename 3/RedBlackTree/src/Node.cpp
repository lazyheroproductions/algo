#include "Node.h"
#include <cstddef>

Node::Node()
{
    //ctor
}

Node::Node(int k, std::string v)
{
    this->key = k;
    this->value = v;
}

Node::Node(bool nil){
    this->key = -1;
    this->value = "NilNode";
    this->isRed = false;
    leftChildNode = rightChildNode = parentNode = this;
}

bool Node::hasChilds(){
    return this->leftChildNode != NULL || this->rightChildNode != NULL;
}
