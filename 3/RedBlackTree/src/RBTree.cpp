#include "RBTree.h"
#include <iostream>

using namespace std;

void RBTree::leftRotate(Node* n){
    if (n->rightChildNode == nil){
        return;
    }
    Node* y = n->rightChildNode;
    n->rightChildNode = y->leftChildNode;
    if (y->leftChildNode != nil){
        y->leftChildNode->parentNode = n;
    }
    y->parentNode = n->parentNode;

    if (n->parentNode == nil)
    {
        //n ist die Wurzel
        rootNode = y;
    }else{
        /*n ist linkes Kind*/
        if (n == n->parentNode->leftChildNode){
            n->parentNode->leftChildNode = y;
        }else{
            n->parentNode->rightChildNode = y;
        }
    }
    y->leftChildNode = n;
    n->parentNode = y;
}

void RBTree::rightRotate(Node* n){
    if (n->leftChildNode == nil){
        return;
    }
    Node* x = n->leftChildNode;
    n->leftChildNode = x->rightChildNode;
    if (x->rightChildNode != nil){
        x->rightChildNode->parentNode = n;
    }
    x->parentNode = n->parentNode;

    if(n->parentNode == nil){
        //n ist die Wurzel
        rootNode = x;
    }else{
        /*n ist linkes Kind*/
        if (n == n->parentNode->leftChildNode){
            n->parentNode->leftChildNode = x;
        }else{
            n->parentNode->rightChildNode = x;
        }
    }
    x->rightChildNode = n;
    n->parentNode = x;
}

void RBTree::insert(Node *newNode){

    Node* y = nil;
    Node* x = rootNode;

    while(x != nil){
        y = x; //Vorgaenger merken
        if (newNode->key < x->key){
            //Gehe Linke Seite runter
            x = x->leftChildNode;
        }else{
            //Gehe Rechte Seite runter
            x = x->rightChildNode;
        }
    }

    newNode->parentNode = y;
    if (y == nil)
    {
        //Baum leer = neue Wurzel
        rootNode = newNode;
    }else
    {
        if (newNode->key < y->key){
            y->leftChildNode = newNode;
        }else{
            y->rightChildNode = newNode;
        }
    }

    /*Knoten n ist eingefuegt*/
    newNode->leftChildNode = nil;
    newNode->rightChildNode = nil;
    newNode->isRed = true;

    repairInsert(newNode);
}

void RBTree::repairInsert(Node* n){

    while (n->parentNode->isRed){
        if (n->parentNode == n->parentNode->parentNode->leftChildNode){
            Node* uncl = n->parentNode->parentNode->rightChildNode;
            if (uncl->isRed){
                n->parentNode->isRed = false;
                uncl->isRed = false;
                n->parentNode->parentNode->isRed = true;
                n = n->parentNode->parentNode;
            }else{
                if (n == n->parentNode->rightChildNode){
                    n = n->parentNode;
                    leftRotate(n);
                }
                n->parentNode->isRed = false;
                n->parentNode->parentNode->isRed = true;
                rightRotate(n->parentNode->parentNode);
            }
        }else{
            Node* uncl = n->parentNode->parentNode->leftChildNode;
            if (uncl->isRed){
                n->parentNode->isRed = false;
                uncl->isRed = false;
                n->parentNode->parentNode->isRed = true;
                n = n->parentNode->parentNode;
            }else{
                if (n == n->parentNode->leftChildNode)
                {
                    n = n->parentNode;
                    rightRotate(n);
                }
                n->parentNode->isRed = false;
                n->parentNode->parentNode->isRed = true;
                leftRotate(n->parentNode->parentNode);
            }

        }
    }
    rootNode->isRed = false;
}

    //Helper Function, with only int Parameter
string RBTree::search(int key){
    return search(rootNode, key);
}

string RBTree::search(Node *root, int key){
    string v;

    if (root == nil || root == NULL){
        v = "NOT FOUND";
    }else if (key == root->key){
        v = root->value;
    }else if (key < root->key){
        v = search(root->leftChildNode, key);
    }else if (key > root->key){
        v = search(root->rightChildNode, key);
    }
    return v;
}

bool RBTree::check(){
    if (rootNode->isRed){
        return false;
    }
    if (checkRedHasBlackChild(rootNode) == false){
        return false;
    }
    if (checkProperty5(rootNode) == false){
        return false;
    }
    return true;
}

bool RBTree::checkRecursive(Node* node){
    if(!node->hasChilds()){
        return true;
    }
    // TODO check this
    // needs nullcheck
    bool temp = false;
    bool tempTwo = false;

    if(node->rightChildNode != NULL &&
       node->key <= node->rightChildNode->key){
        temp = checkRecursive(node->rightChildNode);
    }else if(node->rightChildNode == NULL){
        temp = true;
    }else{
        return false;
    }

    if(node->leftChildNode != NULL &&
       node->key > node->leftChildNode->key){
        tempTwo = checkRecursive(node->leftChildNode);
    }else if(node->leftChildNode == NULL){
        tempTwo = true;
    }else{
        return false;
    }

    return temp && tempTwo;
}

//Red nodes only have black children
bool RBTree::checkRedHasBlackChild(Node* root)
{
    if (root == nil || root == NULL){
        return true;
    }

    if (root->isRed){
        if (root->leftChildNode->isRed == true &&
            root->rightChildNode->isRed == true &&
            root->parentNode->isRed == true){
            return false;
        }
    }
    return checkRedHasBlackChild(root->leftChildNode) && checkRedHasBlackChild(root->rightChildNode);
}

//Property 5: All paths from any given node to its leaf nodes contain the same number of black nodes
bool RBTree::checkProperty5(Node* root){
    int blackCountTotal = -1;
    return checkProperty5Routine(root, 0, &blackCountTotal);
}

bool RBTree::checkProperty5Routine (Node* root, int blackCount, int* blackCountTotal){
    if (root->isRed == false){
        blackCount++;
    }

    if (root == nil || root == NULL){
        if (*blackCountTotal == -1){
            *blackCountTotal = blackCount;
            return true;
        }else{
            if (*blackCountTotal != blackCount){
                return false;
            }else{
                return true;
            }
        }
    }

    if (checkProperty5Routine(root->leftChildNode, blackCount, blackCountTotal) == false ||
        checkProperty5Routine(root->rightChildNode, blackCount, blackCountTotal) == false){
        return false;
    }else{
        return true;
    }
}

int RBTree::height(){
    // max. Stufe eines Knotens im Baum
    return heightRecursive(rootNode);
}

int RBTree::heightRecursive(Node* node){
    if (node == nil || node == NULL){
        return 0;
    }
    return 1 + max(heightRecursive(node->leftChildNode), heightRecursive(node->rightChildNode));
}

int RBTree::max(int a, int b){
    return a > b ? a : b;
}


void RBTree::printTree(Node* node){
    if(node == NULL){
        return;
    }
    cout << node->key << endl;
    printTree(node->leftChildNode);
    printTree(node->rightChildNode);
}

Node* RBTree::findLeftMin(Node* root){
    while(root->leftChildNode != NULL)
        root = root->leftChildNode;

    return root;
}
