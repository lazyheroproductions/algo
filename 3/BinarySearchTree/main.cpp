#include <iostream>
#include <string>
#include <sstream>
#include "Utilities.h"
#include "Random.h"
#include "BSTree.h"
#include "Node.h"

void fillTree();
void shuffleArray(int *a, int n);

using namespace std;

int main(int argc, const char * argv[])
{
    cout << "Hello BSTree!" << endl;
    fillTree();
    return 0;
}

void fillTree(){
    // mit werten zwischen 0 und 1 000 000 füllen
    int length = 1000000;
    for(int j = 1; j < 6; j++){
        cout << "aufbau mit " << length*j << " elementen" << endl;
        BSTree *tree = new BSTree();

        cout << "permutieren" << endl;
        int *a = new int[length * j];
        shuffleArray(a, length * j);

        cout << "baum fuellen" << endl;
        clock_t start = clock();
        for(int i=0; i<length*j; i++) {
            stringstream s;
            s << "knoten_" << (a[i]/10);
            tree->insert(new Node(a[i], s.str()));
            //cout << a[i] << endl;
        }
        clock_t end = clock();
        cout << "zeit gestoppt" << endl;

        double t = (end-start) / 1000;
        cout << "in " << t << "ms gebaut" << endl;

        if(tree->checkRecursive2(tree->rootNode, 0, length*j)){
            cout << "baum ist richtig" << endl;
        }else{
            cout << "baum ist nicht richtig" << endl;
        }
        cout << "hoehe: " << tree->height() << endl;


        for(int i = 0; i < length * j; i+=2){
            tree->remove(i);
        }
        if(tree->checkRecursive2(tree->rootNode, 0, length*j)){
            cout << "baum ist immer noch richtig" << endl;
        }else{
            cout << "baum ist nicht richtig" << endl;
        }

        delete tree;
        delete a;
        cout << "aufgeraeumt" << endl;
    }
}

void shuffleArray(int *a, int n){
    Random r(n-1);

    ///fill array
    for(int i = 0;i<n;i++)
    {
        a[i] = i;
    }

//    for(int i = 0; i<n*10; i++)
//    {
//        swap(a[r.give()], a[r.give()]);
//    }

}
