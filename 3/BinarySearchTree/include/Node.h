#ifndef NODE_H
#define NODE_H

#include <string>

class Node
{




    public:
        Node();
        Node(int k, std::string v);

        bool hasChilds();

        //properties
        int key;
        Node* rightChildNode; // the bigger child
        Node* leftChildNode; // the smaller child
        Node* parentNode;
        std::string value;

    protected:
    private:

};

#endif // NODE_H
