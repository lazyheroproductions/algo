#ifndef BSTREE_H
#define BSTREE_H

#include "Node.h"
#include <cstddef>
#include <string>

class BSTree
{
    public:

        Node* rootNode = NULL;

        // public methods
        void insert(Node *newNode);
        std::string search(int k);
        void remove(int k);
        Node* removeRecursive(Node* root, int data);
        bool check();
        bool checkRecursive(Node* node);
        bool checkRecursive2(Node* node, int min, int max);
        int height();
        int heightRecursive(Node* noe);
        int max(int a, int b);
        void printTree(Node* node);
        Node* findLeftMin(Node* root);
    protected:
    private:

};

#endif // BSTREE_H
