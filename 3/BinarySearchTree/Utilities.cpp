#include "Utilities.h"

    using namespace std;

    /**
    read integer from file and write them into an array.
    the numbers have to be seperated by a newline

    @param *a , pointer to the int array
    @param n , length of the int array
    @param fileName , which file to read from
*/
void Utilities::readFromFile(int *a, std::string fileName){
    cout << "lese zahlen aus datei" << endl;
    string line;
    int i = 0;
    ifstream file(fileName.c_str());
    while (getline(file, line)){
        a[i] = atoi(line.c_str()); // parse string to int
        i++;
    }
    cout << "Zahlen gelesen" << endl;
}

/**
    writes an integer array to a file

    @param *a , pointer to the int array
    @param n , length of the array
    @param fileName , the file to write into
*/
void Utilities::writeToFile(int *a, int n, std::string fileName) {
    cout << "schreibe in datei" << endl;
    ofstream file;
    file.open(fileName.c_str(), std::ios::out); // second parameter to overwrite data
    for (int i = 0; i < n; i++) {
        file << a[i] << "\n";
    }
    file.close();
    cout << "fertig geschrieben" << endl;
}

/**
    counts the amount of lines in a .txt file

    @param fileName , which file to read from
*/
int Utilities::countLineNumbers(std::string fileName){
    int count = 0;
    string line;
    ifstream file(fileName.c_str());
    while (getline(file, line)){
        ++count;
    }
    return count;
}

/**
    writes random integers to files

    @param count , how many ints to generate
    @param fileName , name of the file to write to
*/
void Utilities::createRandomIntegerFile(int count, std::string fileName){
    cout << count << " zahlen werden generiert" << endl;
    int *someIntegers = new int[count];
    Random r(count);
    for (int i = 0; i < count; i++) {
        someIntegers[i] = r.give();
    }
    writeToFile(someIntegers, count, fileName);
    //cleanup
    delete someIntegers;
}

void Utilities::createRandomIntegerArray(int* a, int length){
    Random r(length);
    for(int i=0; i<length; i++) {
        a[i] = r.give();
    }
}
