#include "BSTree.h"
#include <iostream>

using namespace std;

void BSTree::insert(Node *newNode){
    Node* v = rootNode;
    newNode->leftChildNode = newNode->rightChildNode = NULL;
    while(v != NULL){
        if(newNode->key < v->key){
            if(v->leftChildNode == NULL){
                v->leftChildNode = newNode;
                return;
            }else{
                v = v->leftChildNode;
            }
        }else{
            if(v->rightChildNode == NULL){
                v->rightChildNode = newNode;
                return;
            }else{
                v = v->rightChildNode;
            }
        }
    }
    rootNode = newNode; // neuer knoten = wurzel
}

/*
    search der aufgabe
    gibt inhalt des knotens zurück
*/
string BSTree::search(int k){
    // return element with key k oder null
    Node* v = rootNode;
    while(v!= NULL){
        if(k < v->key){
            v = v->leftChildNode;
        }else if(k == v->key){
            return v->value;
        }else{
            v = v->rightChildNode;
        }
    }
    return NULL;
}

void BSTree::remove(int k){
    removeRecursive(this->rootNode, k);
}

Node* BSTree::removeRecursive(Node* root, int data){
    if(root == NULL){
        return NULL;
    }else if (data < root->key){
        root->leftChildNode = removeRecursive(root->leftChildNode, data);
    }
    else if (data > root->key){
        root->rightChildNode = removeRecursive(root->rightChildNode, data);
    }
    else {
        // Case 1:  No child
        if(root->leftChildNode == NULL && root->rightChildNode == NULL) {
            delete root;
            //Pointer auf NULL
            root = NULL;
        }
        //Case 2: One child
        else if(root->leftChildNode == NULL) {
            Node *temp = root;
            root = root->rightChildNode;
            delete temp;
        }
        else if(root->rightChildNode == NULL) {
            struct Node *temp = root;
            root = root->leftChildNode;
            delete temp;
        }
        // case 3: 2 children
        else {
            Node *temp = findLeftMin(root->rightChildNode);
            root->key = temp->key;
            root->rightChildNode = removeRecursive(root->rightChildNode, temp->key);
        }
    }
    return root;
}

bool BSTree::check(){
    // hat der baum eine wurzel?
    // hat er stufen?
    // evtl maximale höheüberprüfen?
    return checkRecursive(rootNode);
}

bool BSTree::checkRecursive(Node* node){
    if(!node->hasChilds()){
        return true;
    }
    // TODO check this
    // needs nullcheck
    bool temp = false;
    bool tempTwo = false;

    if(node->rightChildNode != NULL &&
       node->key <= node->rightChildNode->key){
        temp = checkRecursive(node->rightChildNode);
    }else if(node->rightChildNode == NULL){
        temp = true;
    }else{
        return false;
    }

    if(node->leftChildNode != NULL &&
       node->key > node->leftChildNode->key){
        tempTwo = checkRecursive(node->leftChildNode);
    }else if(node->leftChildNode == NULL){
        tempTwo = true;
    }else{
        return false;
    }

    return temp && tempTwo;
}

bool BSTree::checkRecursive2(Node* node, int min, int max){
    if(node == NULL){
        return true;
    }

    if(node->key < min || node->key > max){
        return false;
    }

    return checkRecursive2(node->leftChildNode, min, node->key - 1) &&
           checkRecursive2(node->rightChildNode, node->key + 1, max);
}

//grad eines knotes ist die anzahl der nachfolgerknoten

int BSTree::height(){
    // max. Stufe eines Knotens im Baum
    return heightRecursive(rootNode);
}

int BSTree::heightRecursive(Node* node){
    if(node == NULL){
        return 0;
    }

    int leftHeight = 0;
    int rightHeight = 0;

    if(node->leftChildNode != NULL){
        leftHeight = heightRecursive(node->leftChildNode);
    }

    if(node->rightChildNode != NULL){
        rightHeight = heightRecursive(node->rightChildNode);
    }

    return max(leftHeight, rightHeight) + 1;
}

int BSTree::max(int a, int b){
    return a > b ? a : b;
}


void BSTree::printTree(Node* node){
    if(node == NULL){
        return;
    }
    cout << node->key << endl;
    printTree(node->leftChildNode);
    printTree(node->rightChildNode);
}

Node* BSTree::findLeftMin(Node* root){
    while(root->leftChildNode != NULL)
        root = root->leftChildNode;

    return root;
}
