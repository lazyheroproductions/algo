#include "Node.h"
#include <cstddef>

Node::Node()
{
    //ctor
}

Node::Node(int k, std::string v)
{
    this->key = k;
    this->value = v;
}

bool Node::hasChilds(){
    return this->leftChildNode != NULL || this->rightChildNode != NULL;
}
