#ifndef EDGE_H
#define EDGE_H


class Edge {
    protected:
        Node *from;
        Node *to;
        Edge *next; // naechste in der Nachbarliste
        double weight; // der Wert bei Kantenbewertung
        ~Edge(){}; // soll nur vom Graphen geloescht werden
    public:
        Edge(Node *v, Node *n, double we=1): from(v), to(n), w(we), next(0), markiert(0), markiert2(0), text("") {}
        friend class Node;
        friend class DirectedGraph;
        Node *vonKnoten() const {return von;}
        Node *nachKnoten() const {return nach;}
        Edge *naechsteKante() const {return naechste;}
        void SetzeWert(double wert) { w = wert ;} // Wert der Kante setzen
        double Wert() const {return w;} // der Wert dieser Kante
        bool markiert, markiert2; // Hilfsfelder f"ur Anwendungen,
        string text; // Algorithmen und Graphik
};

#endif // EDGE_H
