#ifndef DIRECTEDGRAPH_H
#define DIRECTEDGRAPH_H


#include <string>
#include <vector>

class DirectedGraph {
    protected:
        int nodes, edges; // Anzahl Knoten, Kanten
        vector<Node*> allNodes; // Feld aller Knoten
        vector<Edge*> allEdges; // Feld aller Kanten
    public:
        const static int MAXGRAD=10;
        DirectedGraph(int nn=10, int maxgrad=4, bool mitkantenbewertung=false, bool kreisfrei=false); // zufaellig
        DirectedGraph(char *datei="daten/graph1.dat");
        DirectedGraph(const gerichteterGraph&); // tiefe Kopie
        void ausgeben(char *datei="daten/graph0.dat"); //in Datei
        Node* nrKnoten(int knr)const;//der Knoten mit Nummer knr
        int Knotenzahl() const {return n;}; // Anzahl der Knoten
        void knotenausgabe(int bis=50); // sequentiell ausgeben
        int neuerKnoten();
        int neueKante(int von, int nach, double we=1);
    private: // Zuweisung unterbunden
        DirectedGraph& operator=(DirectedGraph&){}
    protected: // Hilfsfunktionen
        int Kantenindex(Kante *kante) const; // Index im Feld
        int Knotenindex(Knoten *knoten) const; // Index im Feld
        void neunummerieren(); // der Knoten im Feld
        void zuruecksetzen(void); // aller Hilfsfelder
};

#endif // DIRECTEDGRAPH_H
