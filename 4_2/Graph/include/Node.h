#ifndef NODE_H
#define NODE_H


class Node {
    protected:
        int nr; // die Knotennummer, fortlaufend ab 1
        Edge *neighbor; // Liste der abgehenden Kanten
        ~Node(){}; // soll nur vom Graphen geloescht werden
    public:
        Node(int num); // au"ser x,y alles auf Null gesetzt
        friend class DirectedGraph;
        int knr(void) const {return nr;}
        Edge *firstEdge() const {return neighbor;}
        public: // Hilfsfelder f"ur einige Algorithmen
        // und Anwendungen sowie zur Graphik
        bool markiert, markiert2; // zum Markieren
        int hnr;
        double hwert; // Hilfsnummer, Hilfswert
        string text; // Zur weiteren Bezeichnung des Knotens
        int x,y; // bei Graphik: Position des Knotens in der Ebene
        void reset(void); // einiger Hilfsfelder
        Edge* Neighbor() const {return nachbarn;}
};
#endif // NODE_H
