#include "DirectedGraph.h"

DirectedGraph::DirectedGraph()
{
    //ctor
}


void DirectedGraph::search(){
    p_vorgaenger=0 /* Vorgaenger auf Suchpfad */
    markiere p
    p auf/an Keller/Schlange
    while(Keller/Schlange != leer){
        p=top() /* von Keller/Schlange */
        if(p=z) return p /* Treffer */
        fuehre fuer alle Nachbarn n von p aus {
            if(n nicht markiert){
                markiere n
                n.vorgaenger=p
                n auf/an Keller/Schlange
            }
        }
    }
    return "nicht gefunden";
}

int DirectedGraph::fillFromFile(string fname){
    ifstream file;
    string line;
    file.open(fname.c_str());

    if (!file){
        cout << "Datei konnte nicht geöffnet werden" << endl;
        return 1;
    }

    int i = 0;
    string nodeFrom, nodeTo;
    float weight;

    while (!file.eof()){
        file >> line;

        switch(i%3){
            case 0:
                nodeFrom = line;
                break;
            case 1:
                nodeTo = line;
                break;
            case 2:
                weight = sToF(line);
                adjacentList[nodeFrom].push_back(Edge(nodeTo, weight));
                break;
        }
        i++;
    }

    return 0;
}

void DirectedGraph::startPrompt(){
    while(true){
        cout << "==============================" << endl;
        cout << "Was möchten Sie tun?:" << endl;
        cout << "[1]: Prüfe ob es einen Pfad zwischen Knoten x und ";
        cout << "Knoten y gibt, der über maximal n Kanten läuft." << endl;
        cout << "[2]: Disjktra Algorithmus" << endl;
        cout << "[3]: Beenden" << endl << endl;
        cout << "Ihre Wahl: ";
        int input;
        while (!(cin >> input)){
            cout << "Bitte eine gültige Zahl eingeben: ";
            cin.clear();
            cin.ignore(INT_MAX, '\n');
        }
        if(input == 1){
            findPathPrompt();
        }
        if(input == 2){
            disjktraPrompt();
        }
        if(input == 3)
            break;
    }
}

void DirectedGraph::disjktraPrompt(){
    string from, to;
    cout << "==============================" << endl;
    cout << "Startknoten eingeben: ";
    cin >> from;
    while (adjacentList.find(from) == adjacentList.end()){
        cout << "Ungültiger Knoten, bitte erneut eingeben: ";
        cin >> from;
    }

    dijkstraPath(from, adjacentList);
}

float DirectedGraph::sToF(string s)
{
    float f = 0;;
    stringstream(s)>>f;
    return f;
}
