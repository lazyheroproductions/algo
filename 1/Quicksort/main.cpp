#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include "Random.h"

//#define aufgabe2

using namespace std;

// should be in a header file...
void quicksortIterative(int *a, int n);
void quicksortRecursive(int a[], int l, int r);
int partition(int a[], int l, int r);
int partition2(int a[], int l, int r);
void readFromFile(int *a, string fileName);
void writeToFile(int *a, int n, string fileName);
void createRandomIntegerFile(int length, string fileName);
void createFiles();
int countLineNumbers(string fileName);
void readSortAndWrite(string fileName);
void deleteFiles();
void median(int *a, int l, int r);


int main() {
    // delete file if existing
    // to make sure we have don't have malformed data
    deleteFiles();
    // write random data to files
    // no malformed data (unless we mess something up)
    createFiles();
    // do stuff :D
    readSortAndWrite("1.txt");
    readSortAndWrite("2.txt");
   // readSortAndWrite("3.txt");
    // that was fun
    // lets do it again <3

    readSortAndWrite("1.txt");
    readSortAndWrite("2.txt");
 //   readSortAndWrite("3.txt");

    return 0;
}

/**
    read ints from file, sorts them and writes them back into the array

    @param fileName , the name of the file to read from
*/
void readSortAndWrite(string fileName){
    int length = countLineNumbers(fileName);
    cout << fileName << " has " << length << " numbers" << endl;
    int* array = new int[length];
    readFromFile(array, fileName);
    quicksortIterative(array, length);
    writeToFile(array, length, fileName);
    delete array;
}

/**
    This method creates the files 1.txt with 100.000 random integers,
    2.txt with 1.000.000 random integers and 3.txt with 5.000.000 random
    integers.
*/
void createFiles(){
    //100.000
    createRandomIntegerFile(100000, "1.txt");
    //1.000.000
    createRandomIntegerFile(1000000, "2.txt");
    //5.000.000
    createRandomIntegerFile(5000000, "3.txt");
}

/**
    deletes 1. txt, 2.txt, 3.txt.
    for simplicities sake we don't do error checking.
*/
void deleteFiles(){
    remove("1.txt");
    remove("2.txt");
    remove("3.txt");
}

/**
    writes random integers to files

    @param count , how many ints to generate
    @param fileName , name of the file to write to
*/
void createRandomIntegerFile(int count, string fileName){
    cout << count << " zahlen werden generiert" << endl;
    int *someIntegers = new int[count];
    Random r(count);
    for (int i = 0; i < count; i++) {
        someIntegers[i] = r.give();
    }
    writeToFile(someIntegers, count, fileName);
    //cleanup
    delete someIntegers;
}

/**
    Quicksort

    @param *a , an integer array
    @param n , length of the array
*/
void quicksortIterative(int *a, int n) {
    cout << "sortiere " << n << " zahlen" << endl;
    clock_t start = clock();
    int s = 0;
    int *kl = new int[n];
    int *kr = new int[n];
    int k = 0; // Anz. gespeicherte Bereiche
    int l = 0;
    int r = n -1;
    while (l < r || k > 0) {
        if (l >= r) { // gespeicherten Bereich verwenden
            l = kl[k];
            r = kr[k];
            k = k - 1;
        }
        s = partition(a, l, r);
        if (r - l >= 2) {
            if ((s - l) > (r - s)) { // links merken
                k = k + 1;
                kl[k] = l;
                kr[k] = s - 1;
                l = s + 1;
            } else { // rechts merken
                k = k + 1;
                kl[k] = s + 1;
                kr[k] = r;
                r = s - 1;
            }
        } else {
            l = r; // bei zwei Elementen: Fertig
        }
    }
    // stop (hammer)time!
    clock_t end = clock();
    cout << "zeit gestoppt" << endl;
    int time = ((end-start)*1000)/CLOCKS_PER_SEC;
    cout << "fertig sortiert in " << time << " millisekunden" << endl;
    // cleanup
    delete kl;
    delete kr;
}

/**
    Quicksort, unused

    @param a[] , an integer array
    @param l , position of the first element of the array,
            normally it should be 0
    @param r , position of the last elemen of the array,
            normally it should be (a.length - 1)
*/
void quicksortRecursive(int a[], int l, int r) {
    if (l < r) {
        int s = partition(a, l, r);
        quicksortRecursive(a, l, s - 1);
        quicksortRecursive(a, s + 1, r);
    }
}

/**
 * this method switches the median of the three elements l, r and m
 * to the r position. see number 2 for a better explanation
 *
 * @param *a , integer array
 * @param l , left element of array
 * @param r , right element of array
 */
void median(int *a, int l, int r){
    int m = (l+r)/2;
    int tempR = a[r];
    int tempL = a[l];
    int tempM = a[m];
    int median = max(min(tempR, tempL), min(max(tempR, tempL), tempM));
    if(median == tempL){
        a[l] = a[r];
        a[r] = median;
    }else if(median == tempM){
        a[m] = a[r];
        a[r] = median;
    }
    // else the r element is the median and we don't need to swap
}

/**
 * lomuto partition function
 *
 * @param a[] , an int array
 * @param l , left element
 * @param r , right element
 */
int partition(int a[], int l, int r) {

    #ifdef aufgabe2
    median(a, l, r);
    #endif

    // p is the pivot element
    int i, j, p, t;

    p = a[r];
    i = l;

    for (j = l; j <= r - 1; j++) {
        if (a[j] <= p) {
            //swappity swap
            t = a[j];
            a[j] = a[i];
            a[i] = t;

            i++;
        }
    }

    t = a[i];
    a[i] = a[r];
    a[r] = t;

    return i;
}

// different lomuto partition function
// unused
int partition2(int a[], int l, int r){
    int i, j, p, t;

    p = a[r];
    i = l - 1;

    for(j =l; j <= r-1; j++) {
        if(a[j] <= p) {
            i++;
            //swap
            t = a[j];
            a[j] = a[i];
            a[i] = t;
        }
    }

    t = a[i+1];
    a[i+1] = a[r];
    a[r] = t;

    return i+1;
}

/**
    read integer from file and write them into an array.
    the numbers have to be seperated by a newline

    @param *a , pointer to the int array
    @param n , length of the int array
    @param fileName , which file to read from
*/
void readFromFile(int *a, string fileName){
    cout << "lese zahlen aus datei" << endl;
    string line;
    int i = 0;
    ifstream file(fileName.c_str());
    while (getline(file, line)){
        a[i] = atoi(line.c_str()); // parse string to int
        i++;
    }
    cout << "Zahlen gelesen" << endl;
}

/**
    writes an integer array to a file

    @param *a , pointer to the int array
    @param n , length of the array
    @param fileName , the file to write into
*/
void writeToFile(int *a, int n, string fileName) {
    cout << "schreibe in datei" << endl;
    ofstream file;
    file.open(fileName.c_str(), std::ios::out); // second parameter to overwrite data
    for (int i = 0; i < n; i++) {
        file << a[i] << "\n";
    }
    file.close();
    cout << "fertig geschrieben" << endl;
}

/**
    counts the amount of lines in a .txt file

    @param fileName , which file to read from
*/
int countLineNumbers(string fileName){
    int count = 0;
    string line;
    ifstream file(fileName.c_str());
    while (getline(file, line)){
        ++count;
    }
    return count;
}

/**
 * returns the bigger of the two ints
 * @param a , an integer
 * @param b , another integer
 */
int max(int a, int b){
    return a > b ? a : b;
}

/**
 * returns the bigger of the two ints
 * @param a , an integer
 * @param b , another integer
 */
int min(int a, int b){
    return a < b ? a : b;
}
