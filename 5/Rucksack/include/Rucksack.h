#ifndef RUCKSACK_H
#define RUCKSACK_H

#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iostream>

using namespace std;

struct paket {
    int id;
    int knall;
    int preis;
    int anzahl;
};

typedef vector< vector <int> > matrix;
typedef vector< paket > konstellation;
typedef vector< vector < konstellation > > paketTabelle;

class Rucksack
{
public:
    vector<paket> pakets;
    paketTabelle R;
    matrix W;
    // Aufgabe
    void parseFile(string fname);
    pair<int, konstellation> rucksack(int g, int n, vector<int> gewichte, vector<int> werte, vector<paket> pakete);
    // Transforms
    vector<int>* calcValue(vector<paket> p);
    vector<int>* calcPrice(vector<paket> p);
    vector<int>* calcPakets(matrix k);
    int calcNumbers(vector<paket> p);
    vector<paket>* multiplyPakets(vector<paket> p);
    // Helper
    int countPaket(int gesuchteId, konstellation k);
    void print(konstellation* p, bool compact);
    void print(vector<int>* m, bool newLine=true);
    void print(matrix* m, int g);

};

#endif
