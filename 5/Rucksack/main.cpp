#include <iostream>
#include "Rucksack.h"
#include <numeric>

using namespace std;

int main()
{
    Rucksack* r = new Rucksack();
    r->parseFile("knaller.dat");

    vector<paket>* einzelnePakete = r->multiplyPakets(r->pakets);
    // n, a[] und w[] berechnen
    vector<int>* preise  = r->calcPrice(*einzelnePakete);
    vector<int>* knaller = r->calcValue(*einzelnePakete);
    int numPakets = r->calcNumbers(r->pakets);

    // Ausgabe der Knaller-Tabelle
    cout << "Knaller: " << endl;
    r->print(&(r->pakets), false);

    // Lösung:
    cout << "Paket mit Maximalem Knall: " << endl;
    pair<int, konstellation> loesung = r->rucksack(1000, numPakets, *preise, *knaller, *einzelnePakete);
    r->print(&(loesung.second), true);
    cout << "Knall gesamt: " << loesung.first << endl;

    return 0;
}
