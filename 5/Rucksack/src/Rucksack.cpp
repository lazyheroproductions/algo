#include "Rucksack.h"

using namespace std;

void Rucksack::parseFile(string fname)
{
    ifstream file;
    string line;
    file.open(fname.c_str());
    if (!file)
    {
        cout << "FILE NOT OPEN" << endl;
        return;
    }
    int i = -1;
    vector<int> temp;
    while (!file.eof())
    {
        i++;
        file >> line;
        if(i <= 3)
            temp.push_back(atoi(line.c_str()));

        if(i == 3)
        {
            // Erstelle eintrag in knaller
            paket newPaket;
            newPaket.id     = pakets.size()+1;
            newPaket.anzahl = temp[0];
            newPaket.knall  = temp[1] * temp[2];
            newPaket.preis  = temp[3];
            this->pakets.push_back(newPaket);
            // temp zurücksetzen
            while(temp.size() > 0)
                temp.pop_back();
            // i zurücksetzen
            i = -1;
        }
    }
}
/*
 *  R   -> Rucksack, dort werden die Nummern der mitgenommenen Pakete gespeichert
 *  W   -> Gewichte Tabelle, dort wird das entsprechende Gewicht der Pakete gespeichert
 *  n   -> Anzahl der Pakete
 *  g   -> Kapazität des Rucksacks (max. Gewicht)
 *  gewichte[] -> Gewichte der Pakete, die man mitnehmen kann
 *  werte[] -> Werte der Pakete, die man mitnehmen kann (Knall)
 *
 *  Lösung(W) sollte 8806 sein!
 */
pair<int, konstellation> Rucksack::rucksack(int g, int n, vector<int> gewichte, vector<int> werte, vector<paket> pakete)
{
    // Tabellen R und W mit 0 initialisieren
    for(int i=0; i<=n; i++)
    {
        // eine Zeile
        vector<int>* W_temp = new vector<int>;
        vector<konstellation>* R_temp = new vector<konstellation>;
        // Spalten
        for(int j=0; j<=g; j++)
        {
            R_temp->push_back(*(new konstellation()));
            W_temp->push_back(0);
        }
        // Zeile einfügen
        R.push_back(*R_temp);
        W.push_back(*W_temp);
    }
    // Tabellen R und W berechnen
    for(int k=1; k<=n; k++)
    {
        for(int j=1; j<=g; j++)
        {
            int gewicht = gewichte[k-1];
            int wertmitaktuellem = W[k-1][j-gewichte[k-1]] + werte[k-1];
            int wertvorher = W[k-1][j];
            // Passt Paket in den Rucksack?
            if(gewicht <= j &&
                    wertmitaktuellem > wertvorher)
            {
                // Hinzunahme von k führt zur Wertsteigerung
                // Füge R entsprechendes Paket hinzu
                konstellation* temp = &(R[k-1][j-gewichte[k-1]]);
                temp->push_back(pakete.at(k-1));
                R[k][j] = *temp;
                // Trage Gewicht in W ein
                W[k][j] = wertmitaktuellem;
            }
            else
            {
                // Hinzunahme von k führt nicht zu Wertsteigerung bzw passt nicht mehr in den Rucksack
                // Konstellation von Paketen hat sich nicht verändert
                R[k][j] = R[k-1][j];
                // Gewicht hat sich nicht verändert
                W[k][j] = wertvorher;
            }
        }
    }

    return pair<int, konstellation> (W[n][g], R[n][g]);
}
/*
 *  Berechnet für jedes Knallerpäckchen den Knall (entspricht Wert beim Rucksackproblem)
 */
vector<int>* Rucksack::calcValue(vector<paket> p)
{
    vector<int>* values = new vector<int>;
    // Rechne Anzahl der Knaller im Paket * Durchmesser eines Knallers
    for(int i=0; i<p.size(); i++)
        values->push_back(p[i].knall);

    return values;
}
/*
 *  Gibt einen vector mit den Preisen der Knallerpäckchen zurück (entspricht Gewicht beim Rucksackproblem)
 */
vector<int>* Rucksack::calcPrice(vector<paket> p)
{
    vector<int>* prices = new vector<int>;
    // Preis eines Päckchens steht in der 4ten Spalte
    for(int i=0; i<p.size(); i++)
        prices->push_back(p[i].preis);

    return prices;
}
/*
 *  Erstellt einen vector mit der Anzahl der jeweilig noch vorhandenen Päckchen
 */
vector<int>* Rucksack::calcPakets(matrix k)
{
    vector<int>* pakets = new vector<int>;
    for(int i=0; i<k.size(); i++)
        pakets->push_back(k[i][0]);

    return pakets;
}
/*
 *  Erstellt einen vector mit den Preisen der Päckchen
 */
int Rucksack::calcNumbers(vector<paket> p)
{
    int numPakets = 0;
    // Preis eines Päckchens steht in der 4ten Spalte
    for(int i=0; i<p.size(); i++)
        numPakets += p[i].anzahl;

    return numPakets;
}
/*
 *  Macht aus der "Knaller Tabelle" einen Vektor von einzelnen Paketen
 */
vector<paket>* Rucksack::multiplyPakets(vector<paket> p)
{
    vector<paket>* multiplied = new vector<paket>;

    for(int i=0; i<p.size(); i++)
        for(int j=0; j<p[i].anzahl; j++)
            multiplied->push_back(p[i]);

    return multiplied;
}

/*
 *  Helper
 */
int countPaket(int gesuchteId, konstellation k)
{
    int anzahl = 0;
    for(int i=0; i<k.size(); i++)
    {
        if(k[i].id == gesuchteId)
        {
            anzahl++;
        }
    }
    return anzahl;
}

void Rucksack::print(konstellation* p, bool compact)
{
    if(compact)
    {
        cout << p->at(0).id;
        for(int i=1; i<p->size(); i++)
        {
            cout << ", " << p->at(i).id;
        }
        cout << endl;
    }
    else
    {
        for(int i=0; i<p->size(); i++)
        {
            cout << "Paket Nr. " << i << ":" << endl;
            cout << "\tID: " << p->at(i).id << endl;
            cout << "\tAnzahl: " << p->at(i).anzahl << endl;
            cout << "\tPreis: " << p->at(i).preis << endl;
            cout << "\tKnall: " << p->at(i).knall << endl;
            cout << "-----------" << endl;
        }
    }
}

void Rucksack::print(vector<int>* m, bool newLine)
{
    for(int j=0; j<m->size(); j++)
    {
        cout << m->at(j) << " ";
        if(newLine)
            cout << endl;
    }
}

void Rucksack::print(matrix* m, int g)
{
    cout << "    ";
    for(int i=0; i<=g; i++)
        cout << i <<"   ";
    cout << endl;
    for(int zeile=0; zeile<m->size(); zeile++)
    {
        cout << zeile << "   ";
        for(int spalte=0; spalte<m->at(zeile).size(); spalte++)
        {
            cout << m->at(zeile).at(spalte) << " ";
            if(m->at(zeile).at(spalte) < 10)
                cout << " ";
            if(m->at(zeile).at(spalte) < 100)
                cout << " ";
        }
        cout << endl;
    }
}
