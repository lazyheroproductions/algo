#include <cstdlib>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include "DirectedGraph.h"

using namespace std;

void searchDialog(DirectedGraph *graph)
{

    string start,end;
    int count;

    cout << "Startpunkt: ";
    cin >> start;
    while(!graph->existsNode(start))
    {
        cout << "Knoten existiert nicht" << endl;
        cout << "Startpunkt: ";
        cin >> start;
    }
    cout << "Endpunkt: ";
    cin >> end;
    while(!graph->existsNode(end))
    {
        cout << "Knoten existiert nicht" << endl;
        cout << "Endpunkt: ";
        cin >> end;
    }
    cout << "Schritte: ";
    cin >> count;

    graph->search(start,end,count);
}

void dijkstraDialog(DirectedGraph *graph)
{
    string start, end;

    cout << "Startpunkt: ";
    cin >> start;
    while(!graph->existsNode(start))
    {
        cout << "Knoten existiert nicht" << endl;
        cout << "Startpunkt: ";
        cin >> start;
    }
    cout << "Endpunkt: ";
    cin >> end;
    while(!graph->existsNode(end))
    {
        cout << "Knoten existiert nicht" << endl;
        cout << "Endpunkt: ";
        cin >> end;
    }

    graph->dijkstra(start,end);
}

int main(int argc, char** argv)
{

    string controlFile;
    DirectedGraph *graph = new DirectedGraph();

    if (argc <= 1)
    {
        //Keine Argumente wurden uebergeben
        cout << "Keine Steuerdatei uebergeben, wechsle zu Default: OS_Map.txt" << endl;
        controlFile = "OS_Map.txt";
    }
    else
    {
        controlFile = argv[1];
        cout << "Steuerdatei: " << controlFile << endl;
    }

    graph->fillFromFile(controlFile.c_str());
    graph->printEdges();

    int menue, value=1;
    while (value)
    {
        cout << "[1] Suche"<<endl;
        cout << "[2] Dijkstra"<<endl;
        cout << "[3] Exit"<<endl;
        cout << "Ihre Auswahl?: ";
        cin >> menue;

        switch(menue)
        {
            case 1:
                searchDialog(graph);
                break;
            case 2:
                dijkstraDialog(graph);
                break;
            case 3:
                exit(0);
                break;
            default:
                cout<<"Menüpunkt nicht vorhanden."<<endl;
                break;
        }
    }

    delete graph;
    return 0;
}

