#ifndef _NODE
#define _NODE


#include "Edge.h"
#include <cfloat>
#include <string>
#include <list>

using namespace std;

class Edge;

class Node {
private:
    string name;
    bool marked;
    Node* predecessor;
    double dist;
    
public:
    list<Edge*>* edgesOut;
     
    Node(): name(""), edgesOut(new list<Edge*>), marked(false), predecessor(NULL), dist(DBL_MAX) {};
    Node(const string& text): name(text), edgesOut(new list<Edge*>), marked(false), predecessor(NULL), dist(DBL_MAX){};
    virtual ~Node(){};
    
    void addEdgeOut(Edge* edge) {edgesOut->push_back(edge);};
    void reset();
    void mark() {marked = true;};
    void unmark() {marked = false;};
    
    const string& getName() const {return name;};
    double& getDistance() {return dist;};
    void setDistance(const double& value) {dist = value;};
    void setPre(Node* pre) {predecessor = pre;};
    Node* getPre(){return predecessor;};
    
    bool isMarked() {return marked;};
    
    bool operator < (const Node& e) const {return dist < e.dist; };
    bool operator > (const Node& e) const {return dist > e.dist; }; 
    bool operator == (const Node& e) const {return name == e.getName(); };
};

#endif

