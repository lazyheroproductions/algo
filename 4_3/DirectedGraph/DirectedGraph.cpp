#include "DirectedGraph.h"
#include "PriorityQueue.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <queue>

Edge* DirectedGraph::addEdge(string from, string to, double distance)
{
    Edge* edge;
    if(!getEdge(graph[from], graph[to]))
    {
        edge = new Edge(graph[from], graph[to], distance);
        edges.push_back(edge);
        graph[from]->addEdgeOut(edge);
    }
    return edge;
}

Node* DirectedGraph::addNode(const string& text)
{
    pair< map < string, Node* >::iterator, bool > ret;
    ret = graph.insert(pair< string, Node* >(text,new Node(text)));

    if(ret.second)
    {
        nodes.push_back(ret.first->second);
    }
    return ret.first->second;
}

Edge* DirectedGraph::getEdge(Node* first, Node* second)
{
    list<Edge*> :: iterator it = graph[first->getName()]->edgesOut->begin();
    while (it != graph[first->getName()]->edgesOut->end())
    {
        if ((*it)->getTo() == second)
        {
            return *it;
        }
        else
        {
            it++;
        }
    }
    return NULL;
}

void DirectedGraph::changeDistance(string from, string to, double value)
{
    Edge* edge = getEdge(graph[from], graph[to]);
    edge->setWeight(value);
}

void DirectedGraph::resetNodes()
{
    map<string,Node*> :: iterator it = graph.begin();
    while (it != graph.end())
    {
        (*it).second->reset();
        it++;
    }
}

bool DirectedGraph::existsNode(string location)
{
    Node *loc = graph[location];
    if(loc == NULL)
    {
        return false;
    }
    return true;
}

void DirectedGraph::printRoute(Node* node)
{
    Node* tempNode = node;
    cout << "Route: ";
    while(tempNode->getPre()!=NULL)
    {
        cout << tempNode->getName() << " <- ";
        tempNode = tempNode->getPre();
    }
    cout << tempNode->getName();
    cout << endl;
}

void DirectedGraph::printEdges()
{
    int count = 0;
    list<Edge*> :: iterator it = edges.begin();

    cout << "Verbindungen:" << endl;
    cout << "=======================" << endl;
    while(it != edges.end())
    {
        cout << (*it)->getFrom()->getName() << " -> " << (*it)->getTo()->getName()
             << " " << (*it)->getWeight() << endl;
        it++;
        count++;
    }
    cout << "---" << count << " Kanten wurden erzeugt---" << endl << endl;
}

bool DirectedGraph::fillFromFile(string filename)
{
    ifstream file;
    file.open(filename.c_str());
    string start;
    string target;
    float distance;

    while(!file.eof())
    {
        file >> start >> target >> distance;
        addNode(start);
        addNode(target);
        addEdge(start, target, distance);
    }
    return true;
}

void DirectedGraph::search(string from, string to, int steps)
{
    resetNodes();

    Node* start = graph[from];
    Node* end = graph[to];

    start->setDistance(0);
    start->mark();
    bool found = false;

    queue< Node* > *q = new queue< Node* >;
    Node *v = new Node();

    q->push(start);

    while (!q->empty() && !found)
    {
        v = q->front();
        if(v->getDistance() > steps - 1)
        {
            break;
        }
        q->pop();
        list<Edge*> :: iterator it = v->edgesOut->begin();
        while (it != v->edgesOut->end() && found != true)
        {
            Node* suc = (*it)->getTo();
            if(suc == end)
            {
                found = true;
                suc->setPre(v);
                suc->setDistance(v->getDistance() + 1);
                q->push(suc);
                break;
            }
            if(!suc->isMarked())
            {
                suc->mark();
                suc->setPre(v);
                suc->setDistance(v->getDistance()+1);
                q->push(suc);
            }
            it++;
        }
    }
    if(found)
    {
        v = q->back();
        if(v->getDistance() <= steps)
        {
            cout << "Erfolgreich! Das Ziel "<< to << " wurde in "
                 << v->getDistance() << " Schritt/en erreicht." << endl;
        }
        printRoute(v);
    }
    else
    {
        cout<<"Zielknoten nicht in " << steps << " Schritt/en erreichbar"<<endl;
    }
}

void DirectedGraph::dijkstra(string from, string to)
{
    PriorityQueue *pQueue = new PriorityQueue(nodes.size() + 1);
    Node* start = graph[from];
    Node* ende = graph[to];
    double distance;

    resetNodes();

    list<Node*> :: iterator it = nodes.begin();
    while (it != nodes.end())
    {
        if((*it) == start)
        {
            (*it)->setDistance(0);
        }
        pQueue->insert((*it));
        it++;
    }

    Node* u = NULL;
    while (pQueue->size() > 0)
    {
        u = pQueue->extractMin();
        u->mark();

        for(list<Edge*>::iterator it = u->edgesOut->begin(); it != u->edgesOut->end(); it++)
        {
            if ((*it)->getTo()->isMarked() == false &&
                    (*it)->getTo()->getDistance() > u->getDistance() + (*it)->getWeight())
            {
                (*it)->getTo()->setDistance(u->getDistance() + (*it)->getWeight());
                (*it)->getTo()->setPre(u);
                pQueue->update((*it)->getTo());
            }
        }
    }
    distance = graph[to]->getDistance();
    cout << "Der kürzeste Weg von " << from << " nach " << to << " ist " << distance << endl;
    printRoute(graph[to]);
}
