#ifndef _EDGE
#define _EDGE
#include <string>

using namespace std;

class Node;

class Edge {
private:
    Node* from;
    Node* to;
    double weight;
    
public:
    Edge(): from(NULL), to(NULL), weight(0){};
    Edge(Node* source, Node* destination, double value):
    from(source), to(destination), weight(value) {}; 
    virtual ~Edge(){};

    Node* getFrom() {return from;};
    Node* getTo() {return to;};
    double getWeight() {return weight;};
    void setWeight(double newWeight) {weight = newWeight;}; 
};

#endif


