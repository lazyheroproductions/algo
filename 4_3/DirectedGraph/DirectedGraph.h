#ifndef _DIRECTEDGRAPH_H
#define _DIRECTEDGRAPH_H

#include <map>
#include <string>
#include "Edge.h"
#include "Node.h"

using namespace std;

class DirectedGraph {
private:
    map<string,Node*> graph;
    list<Edge*> edges;
    list<Node*> nodes;

public:
    DirectedGraph(){};
    virtual ~DirectedGraph(){};

    Edge* addEdge(string from, string to, double distance);
    Node* addNode(const string& text);
    Edge* getEdge(Node* first, Node* second);
    void printEdges(void);
    bool existsNode(string location);

    void changeDistance(string from, string to, double value);
    void resetNodes();
    bool fillFromFile(string filename);

    void search (string from, string to, int steps);
    void printRoute(Node* end);
    void dijkstra(string from, string to);
};

#endif


