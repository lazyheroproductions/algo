#include "PriorityQueue.h"

using namespace std;

Node* PriorityQueue::minimum(void) const
{
    return a[1].get_node();
}

Node* PriorityQueue::extractMin(void)
{
    PQElement min = a[1];

    a[1] = a[n];
    down_heap(1);
    n--;

    return min.get_node();
}

int PriorityQueue::size(void)
{
    return n;
}

void PriorityQueue::down_heap(int index)
{
    int ChildIndex = 0;
    int ParentIndex = index;

    while(ParentIndex <= n/2)
    {
        //Wenn das Linke Element kleiner ist als das rechte
        if (a[ParentIndex*2] <= a[ParentIndex*2+1])
        {
            ChildIndex = ParentIndex*2;
        }
        else
        {
            ChildIndex = ParentIndex*2+1;
        }

        //Wenn das aktuelle Element kleiner ist
        if(a[ParentIndex] > a[ChildIndex])
        {
            //Tauschen
            tausche(a[ParentIndex],a[ChildIndex]);
            ParentIndex = ChildIndex;
        }
        else
        {
            break;
        }
    }
}

void PriorityQueue::insert(Node* node)
{
    PQElement anew(node, node->getDistance());
    n++;
    a[n]=anew;
    up_heap(n);
}

void PriorityQueue::up_heap(int index)
{

    while(index/2>0 && a[index]<=a[index/2]) 	//falls aktuelles element kleiner vorgänger
    {
        tausche(a[index],a[index/2]);		//vertausche aktuelles element mit vorgänger
        index/=2;			        //index = vorgänger
    }
}

void PriorityQueue::increase(Node* node, int d)
{
    int i;

    for(i=0; i<=n; i++)
    {
        if(a[i].get_node()==node)  //suche element im baum
            break;
    }

    a[i].set_priority(a[i].get_priority()+d); //setze neue priorität
    if(d>0) //sortiere baum neu je nach richtung der veränderung
        down_heap(i);
    else
        up_heap(i);
}

void PriorityQueue::update (Node* node)
{
    for(int i=1; i < n; i++ )
    {
        if(a[i].get_node() == node)
        {
            a[i].set_node(node);
            a[i].set_priority(node->getDistance());
            up_heap(i);
            break;
        }
    }
}
