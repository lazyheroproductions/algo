#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <iostream>
#include <string>
#include <vector>

#include "Node.h"

using namespace std;

class PQElement
{
        float prioWert;
        Node* node;
public:
	PQElement(){}
	PQElement(Node* node, float prio):prioWert(prio),node(node){}
	~PQElement(){}

	//Getter
	int get_priority (void) const{return prioWert;}
        Node* get_node() const{return node;}
	//Setter
	void set_priority (int p){prioWert = p;}
	void set_node(Node* n){node = n;}

    bool operator< (const PQElement& e)const{
		return prioWert < e.prioWert;
	}
	bool operator> (const PQElement& e)const{
		return prioWert > e.prioWert;
    }
    bool operator<= (const PQElement& e){
		return prioWert <= e.prioWert;
	}
};

class PriorityQueue
{
private:
	int n;//aktuelle Anzahl
	int length;//max Laenge
	PQElement* a;//Feld zur Speicherung

public:
	PriorityQueue(int len){	//Konstruktor
		length = len;
		n = 0;
		a = new PQElement[length];
	}
	~PriorityQueue(){}//Destruktor

	Node* minimum(void) const;
	Node* extractMin(void);
	void insert(Node* node);
    int size(void);
    void down_heap(int index);
    void up_heap(int index);
    void increase(Node* node, int d);
    void update (Node* node);
    void tausche(PQElement& a1, PQElement& a2){
        PQElement tmp = a1;
        a1 = a2;
        a2 = tmp;
	}
};


#endif

