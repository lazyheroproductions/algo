#ifndef PQELEMENT_H
#define PQELEMENT_H


class PQElement {

    int prio; /* hier: p(a) ist eine ganze Zahl */
    int id; // eindeutige id

    public:
        int getPriority() const { return prio; }
        void setPriority(int p) { prio = p; }
        void setId(int i) { id = i; };
        int getId() { return id; };


        bool operator< (const PQElement& e) const{
            return this->prio < e.prio;
        }


};

#endif // PQELEMENT_H
