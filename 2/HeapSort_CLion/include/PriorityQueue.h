#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H


class PriorityQueue {
    int n; /* aktuelle Laenge */
    int length; /* Gesamtlaenge=Aufnahmekapazitaet */
    PQelement *a; /* fuer das Feld */

    public:
        PriorityQueue(int l=1000){
        length=l;
        n=0;
        a=new PQElement[length];
    }

    PQElement& Maximum(void) const;
    PQElement Extract_Max(void);
    void Insert(const PQElement& anew);
}

#endif // PRIORITYQUEUE_H
