#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include "PQElement.h"
#include "ReferenceField.h"

class PriorityQueue {
    int size; /* aktuelle Laenge */
    int length; /* Gesamtlaenge=Aufnahmekapazitaet */
    PQElement *a; /* fuer das Array */
    ReferenceField ref; /* reference */

    public:
        PriorityQueue(int l=1000){
            length=l+1;
            size=0;
            a = new PQElement[length+1];
            ref.setSize(length+1);
        };

        PQElement Extract_Min();
        void insert(PQElement& newElement);
        int actualSize(){return size;};
        PQElement minimum();
        int findById(int id);
        void increase(int id, int d);
        void deleteElement(int id);
        void swapElements(int a, int b);

        // used for reference field
        int findByIdR(int id);
        void increasePriorityR(int id, int d);
        void deleteElementR(int id);

    private:

        void upheap(int i);
        void downheap(int i, int n);

};

#endif // PRIORITYQUEUE_H
