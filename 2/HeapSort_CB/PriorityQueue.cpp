#include "PriorityQueue.h"
#include <iostream>

using namespace std;

PQElement PriorityQueue::minimum(){
    return a[1];
}

PQElement PriorityQueue::Extract_Min(){

    if(size == 0){
        PQElement* temp = new PQElement();
        temp->setPriority(-1);
        temp->setId(-1);
        return *temp;
    }
    PQElement m = minimum();

    PQElement* temp = new PQElement();
    temp->setPriority(m.getPriority());
    temp->setId(m.getId());

    //swap(a[1], a[size]);
    swapElements(1, size);
    size--;
    downheap(1, size);
    return *temp;
}

/**
    sucht die einfuegeposition im n/2
    fuegt das element immer am ende ein
    und dann wird die queue sortiert

*/
void PriorityQueue::insert(PQElement& newElement){
    size++;
    a[size] = newElement;
    ref.setPos(newElement.getId(), size);
    // baum sortieren
    upheap(size);
}

void PriorityQueue::upheap(int size){

    int i = size;
    while(i>0 && a[i/2].getPriority() > a[i].getPriority()){
        //swap(a[i], a[i/2]);
        swapElements(i, i/2);
        i = i/2;
    }
}


void PriorityQueue::downheap(int i, int n){
    int smallest = i;
    int left = 2*i;
    int right = 2*i+1;
    //check left child
    if(left <= n && a[smallest].getPriority() > a[left].getPriority()){
        smallest = left;
    }
    //check right child
    if(right <= n && a[smallest].getPriority() > a[right].getPriority()){
        smallest = right;
    }
    if(smallest != i){
        //swap(a[i],a[smallest]);
        swapElements(i, smallest);
        downheap(smallest, size);
    }
}

/**
 *  Liefert den Index des Elementes mit der gesuchten ID zurueck.
 *  -1 falls es nicht gefunden worden ist
 */
int PriorityQueue::findById(int id) {
    int index = -1;
    for(int i = 1; i< size; i++) {
        if(a[i].getId() == id) {
            return i;
        }
    }
    return index;
}

/**
 * increase the priority of the element with id by d
 */
void PriorityQueue::increase(int id, int d) {
    int index = findById(id);
    if(index > 0){
        a[index].setPriority(a[index].getPriority() + d );
        if(d > 0) {
            this->downheap(index, size);
        }
        if(d < 0) {
            this->upheap(index);
        }
    }
}

/*
 * deletes the element with the given id
 */
void PriorityQueue::deleteElement(int id){
    int index = findById(id);
    if(index >= 0) {
        //swap(a[index], a[size]);
        swapElements(index, size);
        size--;
        this->downheap(index, size);
    }
}

int PriorityQueue::findByIdR(int id) {
    return this->ref.getPos(id);
}

void PriorityQueue::increasePriorityR(int id, int d) {
    int index = findByIdR(id);
    a[index].setPriority(a[index].getPriority() + d );
    if(d > 0) {
        this->downheap(index, size);
    }
    if(d < 0) {
        this->upheap(index);
    }
}

void PriorityQueue::deleteElementR(int id) {
    int index = findByIdR(id);
    if(index >= 0) {
        //swap(a[index], a[size]);
        swapElements(index, size);
        size--;
        this->downheap(index, size);
    }
}

/**
 *  Vertauscht Elemente an Indizes ea und eb in a und ref
 */
void PriorityQueue::swapElements(int ea, int eb) {
    // Werte in r vertauschen
    ref.swapId(a[ea].getId(), a[eb].getId());
    // Werte in a vertauschen
    swap(a[ea], a[eb]);
}
