#include <iostream>
#include "PriorityQueue.h"
#include "Utilities.h"

void sortieren(int i, string name);
void sortierenRef(int i, string name);
void printArray(int* a, int length);

using namespace std;



int main()
{
    Utilities *u = new Utilities();
    int count = 100000;
    //u->createRandomIntegerFile(count, "1.txt");
    sortieren(count, "1.txt");

    delete u;
    return 0;
}

// aufgabe 1
void sortieren(int length, string fileName){
    Utilities *u = new Utilities();
    int *someIntegers = new int[length];
    //u->readFromFile(someIntegers, "1.txt");
    u->createRandomIntegerArray(someIntegers, length);

    PriorityQueue* queue = new PriorityQueue(length);
    for(int i = 0; i < length; i++){
        PQElement e;
        e.setPriority(someIntegers[i]);
        e.setId(i);
        queue->insert(e);
    }

    clock_t start = clock();
    for(int i = 0; i < length; i++){
        someIntegers[i] = queue->Extract_Min().getPriority();
    }
    clock_t end = clock();

    double t = (end-start) / 1;
    cout << "in " << t << " sortiert" << endl;

    //u->writeToFile(someIntegers, length, fileName);
    delete u;
}


// aufgabe 2
void sortierenRef(int length, string fileName){
    Utilities *u = new Utilities();
    Random r(length);
    int *someIntegers = new int[length];
    int *someIntegersRef = new int[length];
    //u->readFromFile(someIntegers, "1.txt");
    u->createRandomIntegerArray(someIntegers, length);
    u->createRandomIntegerArray(someIntegersRef, length);

    cout << "fill queue 1" << endl;
    //queues fuellen
    PriorityQueue* queue = new PriorityQueue(length);
    for(int i = 0; i < length; i++){
        PQElement e;
        e.setPriority(someIntegers[i]);
        e.setId(i);
        queue->insert(e);
    }

    cout << "fill queue 2 with ref" << endl;
    //mit reference
    PriorityQueue* queueRef = new PriorityQueue(length);
    for(int i = 0; i < length; i++){
        PQElement e;
        e.setPriority(someIntegersRef[i]);
        e.setId(i);
        queueRef->insert(e);
    }
    cout << "queues filled" << endl;
    //######################
    // queues reordern

    cout << "queue size " << queue->actualSize() << endl;
    cout << "reorder queue" << endl;

    clock_t start = clock();
    for(int i=1; i<length; i++ ) {
        if( i%2 == 0 ) {
            queue->deleteElement(i);
        }else{
            queue->increase(i, r.give()/2);
        }
    }
    clock_t end = clock();
    cout << "queue size " << queue->actualSize() << endl;
    int queueSize = queue->actualSize();

    /*
    for(int i = 0; i < queueSize; i++){
        cout << queue->Extract_Min().getPriority() << endl;
    }
    */


    double t = (end-start) / CLOCKS_PER_SEC;
    cout << "queue without ref sorted in " << t << endl;

    cout << "queueRef size " << queueRef->actualSize() << endl;
    cout << "reorder queue with ref list" << endl;
    clock_t startRef = clock();
    for(int i=1; i < length; i++ ) {
        if( i%2 == 0 ) {
            queueRef->deleteElementR(i);
        }else{
            queueRef->increasePriorityR(i, r.give()/2);
        }
    }
    clock_t endRef = clock();

    double tRef = ((endRef-startRef) / CLOCKS_PER_SEC);
    cout << "queueRef size " << queueRef->actualSize() << endl;
    cout << "queue with ref sorted in " << tRef << endl;

    int queueRefSize = queueRef->actualSize();

    /*
    for(int i = 0; i < queueRefSize; i++){
        cout << queueRef->Extract_Min().getPriority() << endl;
    }
    */

    delete u;
}

void printArray(int* a, int length){
    for(int i = 0; i < length; i++){
        cout << a[i] << endl;
    }
}
