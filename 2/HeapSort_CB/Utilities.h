#ifndef UTILITIES_H
#define UTILITIES_H

#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include "Random.h"

class Utilities
{
    public:
        void readFromFile(int *a, std::string fileName);
        void writeToFile(int *a, int n, std::string fileName);
        int countLineNumbers(std::string fileName);
        void createRandomIntegerFile(int count, std::string fileName);
        void createRandomIntegerArray(int* a, int length);
    protected:
    private:
};

#endif // UTILITIES_H
