#ifndef HEAPSORT_REFERENCEFIELD_H
#define HEAPSORT_REFERENCEFIELD_H


class ReferenceField {

    public:

        void setSize(int size) { ref = new int[size];};
        void setPos(int id, int pos);
        int getPos(int id);
        void swapId(int ida, int idb);

    protected:
    private:
        int *ref;
};


#endif //HEAPSORT_REFERENCEFIELD_H
