#include <iostream>
#include "ReferenceField.h"

using namespace std;
void ReferenceField::setPos(int id, int pos) {
    if(id >= 0){
        ref[id] = pos;
    }
}

void ReferenceField::swapId(int ida, int idb){
    swap(ref[ida], ref[idb]);
}

int ReferenceField::getPos(int id){
    if(id >= 0) {
        return ref[id];
    } else {
        return -1;
    }
}